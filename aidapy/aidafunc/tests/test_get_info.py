from aidapy import get_mission_info


def test_get_mms_name():
    mms_info = get_mission_info('mms')
    name = mms_info['name']
    assert name == 'mms'


def test_get_mms_allowed_probed():
    mms_info = get_mission_info('mms')
    allowed_probes = mms_info['allowed_probes']
    assert allowed_probes == ['1', '2', '3', '4']


def test_get_mms_data_settings():
    mms_info = get_mission_info('mms')
    data_settings = mms_info['data_settings']
    true_data_settings = ['dc_mag', 'i_dens', 'e_bulkv', 'i_dist']
    assert all(x in data_settings for x in true_data_settings)


def test_get_cluster_name():
    cluster_info = get_mission_info('cluster')
    name = cluster_info['name']
    assert name == 'cluster'


def test_get_cluster_allowed_probed():
    cluster_info = get_mission_info('cluster')
    allowed_probes = cluster_info['allowed_probes']
    assert allowed_probes == ['1', '2', '3', '4']


def test_get_cluster_data_settings():
    cluster_info = get_mission_info('cluster')
    data_settings = cluster_info['data_settings']
    true_data_settings = ['dc_mag', 'i_dens', 'i_dist']
    assert all(x in data_settings for x in true_data_settings)


def test_get_omni_name():
    omni_info = get_mission_info('omni')
    name = omni_info['name']
    assert name == 'omni'


def test_get_omni_allowed_probed():
    omni_info = get_mission_info('omni')
    allowed_probes = omni_info['allowed_probes']
    assert allowed_probes == ['1']


def test_get_omni_data_settings():
    omni_info = get_mission_info('omni')
    data_settings = omni_info['data_settings']
    true_data_settings = ['dc_mag', 'i_dens', 'all']
    assert all(x in data_settings for x in true_data_settings)
