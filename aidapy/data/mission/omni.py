#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" This module serves as data manager for Omniweb

Authors : Romain Dupuis, Hugo Breuillard """

from collections import OrderedDict

import astropy.units as u
import heliopy.data.omni as omni

from aidapy.data.mission.base_mission import BaseMission


class Omni(BaseMission):
    """Omniweb data manager

    Parameters
    ----------
    t_start : datetime object
        It gives the time at which we started to look at the data

    t_end : datetime object
        It gives the time at which we finished looking at the data

    Attributes
    ----------
    meta_data : metadata object
        Contains all the metadata. It is generated after the data

    ts : timeseries object
        Contains the time series queried by the user

    mission : str
        Name of the misson
    """

    names = ['Year', 'Decimal Day', 'Hour', 'Bartels Rotation Number',
             'ID IMF Spacecraft', 'ID SW Plasma Spacecraft',
             'points(IMF Average)', 'points(Plasma Average)',
             '|B|', 'Magnitude of Avg Field Vector',
             'Lat. Angle of Aver. Field Vector',
             'Long. Angle of Aver. Field Vector', 'Bx GSE, GSM', 'By GSE',
             'Bz GSE', 'By GSM', 'Bz GSM', 'sigma |B|', 'sigma B', 'sigma Bx',
             'sigma By', 'sigma Bz', 'Proton Temperature',
             'Proton Density', 'Plasma Flow Speed', 'Plasma Flow Long. Angle',
             'Plasma Flow Lat. Angle', 'Na/Np', 'Flow Pressure', 'sigma T',
             'sigma N', 'sigma V', 'sigma phi V', 'sigma theta V',
             'sigma Na/Np', 'Electric Field', 'Plasma Beta',
             'Alfven Mach Number', 'Kp', 'R', 'DST Index', 'AE Index',
             'Proton Flux > 1MeV', 'Proton Flux > 2MeV',
             'Proton Flux > 4MeV', 'Proton Flux > 10MeV',
             'Proton Flux > 30MeV',
             'Proton Flux > 60MeV', 'flag', 'ap index',
             'f10.7 index', 'PC(N) index', 'AL index (Kyoto)',
             'AU index (Kyoto)', 'Magnetosonic Mach No.']
    sfu = u.def_unit('sfu', 10**-22 * u.m**-2 * u.Hz**-1)
    units = OrderedDict([('Bartels Rotation Number', u.dimensionless_unscaled),
                         ('ID IMF Spacecraft', u.dimensionless_unscaled),
                         ('ID SW Plasma Spacecraft', u.dimensionless_unscaled),
                         ('points(IMF Average)', u.dimensionless_unscaled),
                         ('points(Plasma Average)', u.dimensionless_unscaled),
                         ('|B|', u.nT),
                         ('Magnitude of Avg Field Vector', u.nT),
                         ('Lat. Angle of Aver. Field Vector', u.deg),
                         ('Long. Angle of Aver. Field Vector', u.deg),
                         ('Bx GSE, GSM', u.nT),
                         ('By GSE', u.nT),
                         ('Bz GSE', u.nT),
                         ('By GSM', u.nT),
                         ('Bz GSM', u.nT),
                         ('sigma |B|', u.nT),
                         ('sigma B', u.nT),
                         ('sigma Bx', u.nT),
                         ('sigma By', u.nT),
                         ('sigma Bz', u.nT),
                         ('Proton Temperature', u.K),
                         ('Proton Density', u.cm**-3),
                         ('Plasma Flow Speed', u.km / u.s),
                         ('Plasma Flow Long. Angle', u.deg),
                         ('Plasma Flow Lat. Angle', u.deg),
                         ('Na/Np', u.dimensionless_unscaled),
                         ('Flow Pressure', u.nPa),
                         ('sigma T', u.K),
                         ('sigma N', u.cm**-3),
                         ('sigma V', u.km / u.s),
                         ('sigma phi V', u.deg),
                         ('sigma theta V', u.deg),
                         ('sigma Na/Np', u.dimensionless_unscaled),
                         ('Electric Field', u.mV / u.m),
                         ('Plasma Beta', u.dimensionless_unscaled),
                         ('Alfven Mach Number', u.dimensionless_unscaled),
                         ('Kp', u.dimensionless_unscaled),
                         ('R', u.dimensionless_unscaled),
                         ('AE Index', u.nT),
                         ('DST Index', u.nT),
                         ('Proton Flux > 1MeV', u.cm**-2),
                         ('Proton Flux > 2MeV', u.cm**-2),
                         ('Proton Flux > 4MeV', u.cm**-2),
                         ('Proton Flux > 10MeV', u.cm**-2),
                         ('Proton Flux > 30MeV', u.cm**-2),
                         ('Proton Flux > 60MeV', u.cm**-2),
                         ('flag', u.dimensionless_unscaled),
                         ('ap index', u.nT),
                         ('PC(N) index', u.dimensionless_unscaled),
                         ('AL index (Kyoto)', u.nT),
                         ('AU index (Kyoto)', u.nT),
                         ('Magnetosonic Mach No.', u.dimensionless_unscaled),
                         ('f10.7 index', sfu)])

    def __init__(self, t_start, t_end):
        super().__init__(t_start, t_end)
        self.mission = 'omni'
        self.allowed_probes = ['1']
        self.probes = ['1']
        self._probe = '1'
        self.coords = 'gse'
        self.data_types = ['all']

    def _download_ts(self):
        omni_df = omni.low(self.t_start.datetime, self.t_end.datetime,
                           product_list=self.product_list, want_xr=True)
        return omni_df

    def set_observation(self, obs_settings):
        """Doc string
        """
        self.product_list = obs_settings['prod']

    def _json_replace(self, json_str):
        json_str = json_str.replace('${coords}', self.coords.upper())
        #json_str = json_str.replace('${name_all}', str(self.names[3:])[1:-1])
        return json_str

    def _set_mode_default(self, obs_settings):
        """Set the mode specific to each instrument.
        """
        self._mode = None

    @classmethod
    def variables_info(cls):
        r"""Print variables info with corresponding index
        """
        print('This is the list of variables downloaded from OMNIWEB')
        for variable in cls.names:
            print(variable)
            print('-----------------------------')
