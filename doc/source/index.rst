.. aidapy documentation master file, created by
   sphinx-quickstart on Tue Apr  2 12:33:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AIDApy's documentation!
==================================

.. include:: ../../README.rst
   :end-before: end-marker-intro-do-not-remove


Getting Started
-----------------

* :doc:`motivations`
* :doc:`installing`

..
	* :doc:`examples`

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Getting Started

   motivations
   installing
..
   examples

Reference manual
-----------------

* :doc:`mission`
* :doc:`event_search`
* :doc:`ml`

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Reference manual

   mission
   event_search
   ml


Examples
----------
In this section, we provide simple and practical examples on how to use
the different subpackages of the ``aidapy`` package.

.. toctree::
   :glob:
   :maxdepth: 1

   ../examples/01_missions/mission_test.ipynb
   ../examples/01_missions/event_search_mms_edr.ipynb
   ../examples/01_missions/vdf_mms_overview.ipynb
   ../examples/02_omni_regressor/index.rst
..   ../examples/03_gmm/index.rst


Modules
---------

.. toctree::
    :maxdepth: 2
    :glob:

    _generated/modules


Help & reference
----------------

* :doc:`contributing`
* :doc:`help`
* :doc:`reference`
* :doc:`about`

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Help & reference

   contributing
   help
   reference
   about
